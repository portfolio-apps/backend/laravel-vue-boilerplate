import {createRouter, createWebHistory} from "vue-router";

import Home from '../pages/HomePage.vue'
import About from '../pages/AboutPage.vue'
import Contact from '../pages/ContactPage.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/about',
        name: 'About',
        component: About
    },
    {
        path: '/contact',
        name: 'Contact',
        component: Contact
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router