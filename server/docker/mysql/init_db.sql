-- Create Databases
CREATE DATABASE IF NOT EXISTS `laraveldb`;
CREATE DATABASE IF NOT EXISTS `laraveldb_test`;

-- Create root user and grant rights
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'laraveldb'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;