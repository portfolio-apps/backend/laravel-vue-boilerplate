<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        $response['success'] = true;
        $response['posts'] = $posts;

        if (request()->is('api/*') == 1) {
            // Returns json data from api routes
            return response()->json($response, 200);
        } else {
            // Will return server side web page from laravel
            return view('posts.index')->with($response);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = Post::create($request->all());

        $response['success'] = true;
        $response['post'] = $post;

        if (request()->is('api/*') == 1) {
            // Returns json data from api routes
            return response()->json($response, 201);
        } else {
            // Will return server side web page from laravel
            return view('posts.create')->with($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post = Post::where('id', $post->id)->get();
        $response['success'] = true;
        $response['post'] = $post;

        if (request()->is('api/*') == 1) {
            // Returns json data from api routes
            return response()->json($response, 200);
        } else {
            // Will return server side web page from laravel
            return view('posts.index')->with($response);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post = Post::where("id", $post->id)->first();
        $post->update($request->all());

        $response['success'] = true;
        $response['post'] = $post;

        if (request()->is('api/*') == 1) {
            // Returns json data from api routes
            return response()->json($response, 200);
        } else {
            // Will return server side web page from laravel
            return view('posts.create')->with($response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post = Post::where('id', $post->id)->first();
        $post->delete();

        $response['success'] = true;

        if (request()->is('api/*') == 1) {
            // Returns json data from api routes
            return response()->json($response, 200);
        } else {
            // Will return server side web page from laravel
            return view('posts.index')->with($response);
        }
    }
}
