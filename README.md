# Fullstack Laravel Vue Boilerplate

## Notes

- API server running at http://localhost:8000
- PHPMyAdmin running at http://localhost:8081
- Vue Application running at http://localhost:8080

## Build Project with Docker

```bash
## From project scope to server directory
cd server
## Builds docker environment
docker-compose up --build
```

### If receive error that refers to laravel.log run the following from the ./server folder (no inside container)

[Link to resolving](https://stackoverflow.com/questions/23411520/how-to-fix-error-laravel-log-could-not-be-opened)

```bash
sudo chown -R $USER:www-data storage
sudo chown -R $USER:www-data bootstrap/cache

chmod -R 775 storage
chmod -R 775 bootstrap/cache
```

## Database Migrations

This below command is used for running commands inside the "app" docker container
`yarn docker:dev:cmd`

### Migrate tables to database
```bash
## Migrate Database Tables
yarn docker:dev:cmd php artisan migrate
```

### Add new model and migration table for model

After running the below command it will generate a template migration file at `./database/migrations`

```bash
### Create new model and include database migration
yarn docker:dev:cmd php artisan make:model Post -m

### Migrate new table to database
yarn docker:dev:cmd php artisan migrate

### Rollback last migration
yarn docker:dev:cmd php artisan migrate:rollback
```

### Seed tables with database seeder

Create factory to generate random seed data located in `./database/factories`

```bash
### Create a da
yarn docker:dev:cmd php artisan make:factory UserFactory
```

Seed database with random data from factories located in `./database/seeds`

```bash
### Create a database seeder
yarn docker:dev:cmd php artisan make:seeder PostSeeder
```

```bash
### Create new model and include database migration
yarn docker:dev:cmd php artisan db:seed
```

```bash
### Run single database seeder with by its class name
yarn docker:dev:cmd php artisan db:seed --class=PostSeeder
```

### Adding API routes

In order to create routes you will add page routes to `./routes/web.php` and API routes to `./routes/api.php`

Follow the below route convention to link them to controller methods.

```php
<?php

use Illuminate\Support\Facades\Route;

// Post routes
Route::get('posts', 'PostController@index');
Route::post('posts', 'PostController@store');
Route::get('posts/{post}', 'PostController@show');
Route::put('posts/{post}', 'PostController@update');
Route::delete('posts/{post}', 'PostController@destroy');
```

### Adding a CRUD controller for Post Routes

These classes will be available in `./app/Http/Controllers`

```bash
yarn docker:dev:cmd php artisan make:controller PostController --model=Post
```

# Frontend Vue Application

Run frontend application

```bash
cd frontend
yarn serve
```